﻿namespace CodeWars.ValidParentheses {
    internal class Parentheses {
        public static bool ValidParentheses(string input) {
            int counter = 0;
            foreach(char c in input) {
                if(c == '(') { counter++; }
                else if(c == ')') { counter--; }
                if(counter < 0) { break; }
            }
            return (counter == 0);
        }
    }
}
