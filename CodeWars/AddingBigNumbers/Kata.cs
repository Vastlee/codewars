﻿using System;

namespace CodeWars.AddingBigNumbers {
    internal class Kata {
        // Breaks numeric strings down to be added together digit by digit.
        // Contatenates them back together, & returns the result. Thus allowing addition of infinite numers.
        public static string Add(string a, string b) {
            string result = String.Empty;
            int[] arrA = NumericStringToInverseInts(a);
            int[] arrB = NumericStringToInverseInts(b);
            int lenAvB = (arrA.Length > arrB.Length) ? arrA.Length : arrB.Length;
            int aNum, bNum, sum, remainder, index;
            aNum = bNum = sum = remainder = index = 0;

            while(index < lenAvB
                || (index >= lenAvB && remainder > 0)) {

                aNum = (arrA.Length > index) ? arrA[index] : 0;
                bNum = (arrB.Length > index) ? arrB[index] : 0;
                sum = aNum + bNum + remainder;
                result = TrimToSingleDigit(sum, out remainder) + result;
                index++;
            }
            return result;
        }

        // Trims a number to the last digit & spits out any remainder
        private static string TrimToSingleDigit(int num, out int remainder) {
            string result = num.ToString();
            remainder = 0;
            if(num > 9) {
                string toStr = num.ToString();
                result = toStr.Substring(toStr.Length-1);
                Int32.TryParse(toStr.Substring(0, toStr.Length - 1), out remainder);
            }
            return result;
        }

        // Converts every digit in a numeric string & inversely puts them into an array
        private static int[] NumericStringToInverseInts(string num) {
            const int ASCII_OFFSET = 48;

            int[] result = new int[num.Length];
            int nextLastDigit = num.Length - 1;

            for(int i = 0; i < num.Length; i++) {
                result[i] = num[nextLastDigit--] - ASCII_OFFSET;
            }
            return result;
        }
        private static int LastDigitToInt(string num) {
            Int32.TryParse(num.Substring(num.Length), out int result);
            return result;
        }
    }
}
