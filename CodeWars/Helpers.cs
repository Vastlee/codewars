﻿using System;

namespace CodeWars {
    internal static class Helpers {
        public static void PrintArrayAsCSV(long[] inArray) {
            for(int i = 0; i < inArray.Length; i++) {
                Console.Write(inArray[i].ToString());
                if(i < inArray.Length - 1) {
                    Console.Write(", ");
                } else {
                    Console.WriteLine();
                }
            }
        }

        public static void PrintArrayAsCSV(int[] inArray) {
            for(int i = 0; i < inArray.Length; i++) {
                Console.Write(inArray[i].ToString());
                if(i < inArray.Length - 1) {
                    Console.Write(", ");
                } else {
                    Console.WriteLine();
                }
            }
        }

        public static void PrintArrayAsCSV(byte[] inArray) {
            for(int i = 0; i < inArray.Length; i++) {
                Console.Write(inArray[i].ToString());
                if(i < inArray.Length - 1) {
                    Console.Write(", ");
                } else {
                    Console.WriteLine();
                }
            }
        }

        public static void PrintCharArrayToNumericAsCSV(char[] inArray) {
            for(int i = 0; i < inArray.Length; i++) {
                Console.Write(char.GetNumericValue(inArray[i]).ToString());
                if(i < inArray.Length - 1) {
                    Console.Write(", ");
                } else {
                    Console.WriteLine();
                }
            }
        }
    }
}
