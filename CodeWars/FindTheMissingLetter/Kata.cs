﻿namespace CodeWars.FindTheMissingLetter {
    internal class Kata {
        public static char FindMissingLetter(char[] array) {
            char result = (char) (array[array.Length-1] + 1);
            for(int i = 0; i < array.Length - 1; i++)
                if((array[i] + 1) != array[i + 1]) {
                    result = (char) (array[i] + 1);
                    break;
                }
            return result;
        }
    }
}
