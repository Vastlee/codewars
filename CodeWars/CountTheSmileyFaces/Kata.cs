﻿namespace CodeWars.CountTheSmileyFaces {
    public static class Kata {
        public static int CountSmileys(string[] smileys) {
            int smileyCount = 0;
            char[] validEyes = new char[] { ':', ';' };
            char[] validNose = new char[] { '-', '~' };
            char[] validSmile = new char[] { ')', 'D' };
            foreach(string element in smileys) {
                if(IsValidFeature(element[0], validEyes)
                    && IsValidFeature(element[element.Length - 1], validSmile)) {
                    if(element.Length < 3)
                        smileyCount++;
                    else if(IsValidFeature(element[1], validNose)) {
                        smileyCount++;
                    }
                }
            }
            return smileyCount;
        }

        private static bool IsValidFeature(char feature, params char[] validChars) {
            foreach(char validChar in validChars)
                if(feature == validChar)
                    return true;
            return false;
        }
    }
}
