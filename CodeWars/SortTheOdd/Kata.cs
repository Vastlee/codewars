﻿using System.Linq;

namespace CodeWars.SortTheOdd {
    internal class Kata {
        public static int[] SortArray(int[] array) {
            int[] oddNums = array
                .Where(n => (n % 2) == 1)
                .OrderBy(n => n)
                .ToArray();

            int nextOdd = 0;
            for(int i = 0; i < array.Length; i++)
                if((array[i] % 2) != 0)
                    array[i] = oddNums[nextOdd++];

            return array;
        }
    }
}
