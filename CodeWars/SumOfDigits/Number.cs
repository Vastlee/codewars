﻿using System;

namespace CodeWars.SumOfDigits {
    internal class Number {
        public static int DigitalRoot(long n) {
            string str = n.ToString();
            int sum = 0;
            RecursiveDigitalRoot(str, ref sum);
            return sum;
        }

        private static void RecursiveDigitalRoot(string numString, ref int sum) {
            if(numString.Length > 1)
                RecursiveDigitalRoot(AddString(numString), ref sum);
            else
                sum = Int32.Parse(numString);
        }

        private static string AddString(string s) {
            int testNum = 0;
            foreach(char c in s)
                testNum += (int) Char.GetNumericValue(c);
            return testNum.ToString();
        }

        internal static void Demo(int n) {
            Console.WriteLine(DigitalRoot(n));
        }
    }
}
