﻿using System;

namespace CodeWars.HighestScoringWord {
    public class Kata {
        private const int ASCII_LETTER_START_VALUE = 96;

        public static string High(string s) {
            string result = String.Empty;
            int highestScore = 0;
            int score = 0;
            string[] sArray = s.Split();
            foreach(string element in sArray) {
                score = WordScore(element);
                if(score > highestScore) {
                    result = element;
                    highestScore = score;
                }
            }
            return result;
        }

        private static int WordScore(string s) {
            int result = 0;
            for(int i = 0; i < s.Length; i++)
                result += (s[i] - ASCII_LETTER_START_VALUE);
            return result;
        }
    }
}
