﻿using System;

namespace FindTheParityOutlier {
    public class Kata {
        public static int Find(int[] integers) {
            int result = integers[0];
            bool lookingForEven = IsMostlyEven(integers);
            foreach(int num in integers) {
                if(IsEven(num)) {
                    if(lookingForEven) { return num; }
                } else if(!lookingForEven) {
                    return num;
                }
            }
            return result;
        }

        private static bool IsMostlyEven(int[] ints) {
            int count = 0;
            count += IsEven(ints[0]) ? -1 : 1;
            count += IsEven(ints[1]) ? -1 : 1;
            count += IsEven(ints[2]) ? -1 : 1;
            return (count > 0);
        }

        private static bool IsEven(int num) {
            return ((num % 2) == 0);
        }

        internal static void Demo(int[] arrayToCheck) {
            Console.Write("Finding Unique Int: ");
            for(int i = 0; i < arrayToCheck.Length; i++) {
                Console.Write(arrayToCheck[i].ToString());
                if((i + 1) < arrayToCheck.Length) {
                    Console.Write(", ");
                }
            }
            Console.WriteLine();
            Console.WriteLine("Result: " + Find(arrayToCheck));
        }
    }
}
