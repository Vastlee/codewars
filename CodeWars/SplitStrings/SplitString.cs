﻿namespace CodeWars.SplitStrings {
    internal class SplitString {
        public static string[] Solution(string str) {
            string[] result = new string[(str.Length / 2) + (str.Length % 2)];
            int entry = 0;
            for(int i = 0; i < str.Length; i++)
                result[entry++] = (i + 1 < str.Length) ? str.Substring(i++, 2) : str[i] + "_";
            return result;
        }
    }
}
