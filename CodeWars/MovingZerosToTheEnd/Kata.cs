﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeWars.MovingZerosToTheEnd {
    internal class Kata {
        public static int[] MoveZeroes(int[] arr) {
            int[] result = new int[arr.Length];
            int resultIndex = 0;
            for(int i = 0; i < arr.Length; i++)
                if(arr[i] != 0)
                    result[resultIndex++] = arr[i];
            return result;
        }
    }
}
