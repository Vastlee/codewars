﻿using System.Collections.Generic;

namespace CodeWars.Scramblies {
    internal class Scramblies {
        public static bool Scramble(string str1, string str2) {
            bool result = false;
            foreach(char c1 in str2) {
                result = false;
                for(int i = 0; i < str1.Length; i++) {
                    if(str1[i] == c1) {
                        result = true;
                        str1 = str1.Substring(0, i) + str1.Substring(i + 1);
                        break;
                    }
                }
                if(!result) { break; }
            }
            return result;
        }
    }
}
