﻿namespace CodeWars.NextBiggerNumber {
    internal class Kata {
        public static long NextBiggerNumber(long n) {
            long result = -1;
            int[] nums = LongToIntArray(n);
            if(!IsLongEnough(nums)) { return -1; }
            int? foundIndex = IndexOfLastAscendingPair(nums);
            if(foundIndex.HasValue) {
                Swap(ref nums[foundIndex.Value], ref nums[IndexOfSmallestFromFound(nums, foundIndex.Value)]);
                System.Array.Sort(nums, (foundIndex.Value + 1), (nums.Length - 1) - foundIndex.Value);
                result = IntArrayToLong(nums);
            }
            return result;
        }

        private static int? IndexOfLastAscendingPair(int[] nums) {
            int? result = null;
            for(int i = (nums.Length - 1); i > 0; i--) {
                if(nums[i] > nums[i - 1]) {
                    result = i - 1;
                    break;
                }
            }
            return result;
        }

        private static int IndexOfSmallestFromFound(int[] nums, int foundIndex) {
            int result = foundIndex + 1;
            for(int i = nums.Length - 1; i >= foundIndex + 1; i--) {
                if(nums[i] > nums[foundIndex] && nums[i] < nums[result]) {
                    result = i;
                }
            }
            return result;
        }

        private static void Swap(ref int firstIndex, ref int secondIndex) {
            int tmp = firstIndex;
            firstIndex = secondIndex;
            secondIndex = tmp;
        }

        private static long IntArrayToLong(int[] nums) {
            long result = 0;
            long multiplier = 1;
            for(int i = nums.Length - 1; i >= 0; i--) {
                result += nums[i] * multiplier;
                multiplier *= 10;
            }
            return result;
        }

        private static int[] LongToIntArray(long num) {
            string str = num.ToString();
            int[] result = new int[str.Length];

            for(int i = 0; i < str.Length; i++) {
                result[i] = (int) System.Char.GetNumericValue(str[i]);
            }
            return result;
        }

        private static bool IsLongEnough(int[] nums) {
            return (nums.Length > 1);
        }
    }
}
