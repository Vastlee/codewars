﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeWars.YouOnlyNeedOne {
    public class Kata {
        public static bool Check(object[] a, object x) {
            foreach(object obj in a) {
                if(obj.ToString().Equals(x.ToString())) {
                    return true;
                }
            }
            return false;
        }
    }
}
