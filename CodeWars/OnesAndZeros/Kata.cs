﻿using System;

namespace OnesAndZeros {
    internal class Kata {
        private static int ConvertBinaryArrayToInt(int[] arr) {
            int result = 0;
            int multiplier = 1;

            for(int i = arr.Length - 1; i >= 0; i--) {
                if(arr[i] == 1) { result += multiplier; }
                multiplier *= 2;
            }
            return result;
        }

        private static int[] RandomArrayOfZerosAndOnes(int len) {
            int[] result = new int[len];
            Random rand = new Random();
            for(int i = 0; i < len; i++) {
                result[i] = rand.Next(2);
            }
            return result;
        }

        internal static void Demo(int len) {
            int[] ranArray = RandomArrayOfZerosAndOnes(len);

            Console.Write("Converting Binary: ");
            for(int i = 0; i < len; i++) {
                Console.Write(ranArray[i].ToString());
            }
            Console.WriteLine();
            Console.WriteLine("Result: " + ConvertBinaryArrayToInt(ranArray));
        }
    }
}
