﻿using System.Collections.Generic;

namespace CodeWars.TripleDouble {
    internal class Kata {
        public static int TripleDouble(long num1, long num2) {
            int result = 0;            
            HashSet<int> doubles = GetDoubles(num2);
            string digits = num1.ToString();

            for(int i = 2; i < digits.Length; i++) {
                if(digits[i] == digits[i - 1] && digits[i] == digits[i - 2]) {
                    if(doubles.Contains(digits[i])) {
                        result = 1;
                        break;
                    }
                }
            }
            return result;
        }

        private static HashSet<int> GetDoubles(long num2) {
            HashSet<int> result = new HashSet<int>();
            string digits = num2.ToString();
            for(int i = 1; i < digits.Length; i++) {
                if(digits[i] == digits[i - 1]) {
                    if(!result.Contains(digits[i])) {
                        result.Add(digits[i]);
                    }
                }
            }
            return result;
        }
    }
}
