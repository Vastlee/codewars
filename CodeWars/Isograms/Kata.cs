﻿using System.Collections.Generic;

namespace Isograms {
    internal class Kata {
        public static bool IsIsogram(string str) {
            str = str.ToLower();
            var testSet = new HashSet<char>();
            for(int i = 0; i < str.Length; i++) {
                if(testSet.Contains(str[i]))
                    return false;
                else
                    testSet.Add(str[i]);
            }
            return true;
        }
    }
}
